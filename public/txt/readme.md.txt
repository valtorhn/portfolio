### Versions: ###

* App(beta):0.1
* FlexboxGrid: 6.3.0
* Embedded icon font from font-awesome 4.6.3

### Sources ###

[BitBucket](https://bitbucket.org/bskydive/wisna_lp)

### Limitations ###

* don't adapted for retina(dpr:2,3) displays
* Nav links are not implemented because of missing it in templates
* Sizes rounded to 1px
* Removed double box style in carousel
* Padding and margin size are not pixel-perfect
* Omitted photoshop shadow "curve" form in carousel boxes
* Avoid using z-index to simplify code
* Removed text-transform with font-size in slider buttons and sidebar header
* Content text boxes min-height 190px and max-height 285px suggested from psd template
* Slider buttons shadow opacity corrected from psd CSS export(0.004) to real(0.9) by image comparison
* Fixed sidebar height to 518px

### Feature list ###

* reviewed at:
chrome 51,52;
ie-edge 12,13;
firefox 48,47;
safari 9,8;
opera 36,37;
* social media integration for embedded view (opengraph, twitter)
* determine noScript
* Long text truncate in content text boxes

### Wish list ###

* JS carousel viewer: slide count, autoplay, loop play
* responsive design
* collapse menu on xs/sm screens(smaller popup font-size/padding/float-left)

